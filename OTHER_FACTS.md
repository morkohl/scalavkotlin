# Facts

## General Comparison

| Point | Scala | Kotlin
| --- | --- | --- |
| Current Version | 2.12.9 | 1.3.50 |
| Release year | 2003 | 2011 |
| Authors | Martin Odersky | JetBrains
| Currently Maintained By | Community* | Jetbrains & Google |
| Paradigm | Object Oriented and Functional | Object Oriented |
| Popular Use cases | Big Data | Alternative to Java |
| Spring Supported | yes | yes |

> A full list of scala maintainers can be found here: https://www.scala-lang.org/old/node/292.html

## Java Interoperability

Both Scala and Kotlin are backwards compatible with Java. Because they were developed with different goals however, Scala with the goal of creating a different language based on the JVM and Kotlin with the goal of creating a "better" Java that needs less boilerplate code, Java interoperability is alot more performant in Kotlin than in Scala

Kotlin offers an Annotation to bridge to Java Code `@JvmStatic` and other Kotlin types can easily be cast to Java types.
```kotlin
val kotlinList: List<Int> = listOf(1, 2, 3)
val javaList: java.util.List<Int> = kotlinList as java.util.List<Int>
println(javaList) // 1, 2, 3
```

```kotlin
fun main() {
    val list = java.util.LinkedList<Int>()
    // works because of automatic cast
    doIt(list)
}


fun doit(l: List<Int>) { }
```

Scala converts collections by using the built-in feature `implicit conversions`. This causes alot more overhead than in Kotlin though.

## Type System

The type system of Kotlin and Scala have alot in common:
- Static Typing
- Classing and Interfaces (Traits)
- Simplification of Type references with `Any`
- `Nothing` as a base-type
- Generic Types with Contravariance and Covariance
- Companion Objects
- No checked exceptions

Scalas type sytem supports more exotic features like:
- Stackable Traits
- Self-Type
- Path Dependend Types
- Structural Typing (Duck Typing)
- Abstract Type Members


Kotlins type system supports `null safety`

Scalas type system may have more features but most of them wont get used when actually developing simple apps.

## IDE Integration

- IntelliJ plugins exist for both Scala and Kotlin
    - Both the Scala and Kotlin plugin are developed by Jetbrains
    - Kotlin plugin is superior due to more frequent feature updates (mainly on stuff like static code analysis, refactorings and inspections)
- Scala has a dedicated IDE (Scala IDE)
    - Sadly dead (no updates since 2017) :(
- A Visual Studio Code plugin exists for Scala

## Build Tools

**Scala** - SBT* (Scala Build Tool), Gradle, Maven

**Kotlin** - Gradle, Maven

> *The Scala build tool is required to compile binaries for different scala versions at a time => no compatibility between scala code written in different versions!* 

## Repl 

### Scala 
`scala`  
 - Auto-Completion
 - Type Inference
 - Load dependencies with `:load com.adsoul.module.MyClass` 
 
 
### Kotlin 
 `kotlinc`
 - Type Inference

## Library Support

https://github.com/lauris/awesome-scala
https://github.com/mcxiaoke/awesome-kotlin
## Community Interest

https://trends.google.de/trends/explore?date=today%205-y&geo=DE&q=%2Fm%2F091hdj,%2Fm%2F0_lcrx4

https://madnight.github.io/githut/#/pushes/2019/2

## Community Opinions

https://www.slant.co/versus/116/1543/~scala_vs_kotlin

https://insights.stackoverflow.com/survey/2019#technology-_-most-loved-dreaded-and-wanted-languages

## Interesting Community Projects

- Scala to Javascript Transpiler "Scala.js" https://www.scala-js.org/

## What does the Future hold for Kotlin and Scala?

Big changes are not to be expected for Kotlin. Of course there will be changes here and there, there will be more frameworks written in Kotlin popping up in the future because of growing community support. But because of the big selling point `Java Interoperability`, Kotlin is kind of limited.

Scala on the other hand is about to receive a new major version with Scala 3.0. The language will receive a new Compiler which will hopefully make compilation faster. The language will also be based on a new principle: DOT (Dependent Object Types). This will add the ability to add new features like Union Types for example. 
