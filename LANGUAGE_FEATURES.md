# Language Features

### Pattern Matching

###### Scala

Scala supports powerful pattern matching by matching on patterns. A pattern can be an exact value match (case classes, as shown below, provide a class-level extension of value matching), an if expression, or a type check.

```scala
case class Person(name: String, age: Int)

val person = Person("Jim", 12)

person match {
  case Person(_, 12) => println(s"${person.name} is 12 years old.")
  case Person(_, _) => println(s"${person.name} is not 12 years old.")
}
```

> In order to compile, a match expression has to be exhaustive. This means that every single possible case has to be matched. 
However, this can easily be achieved by using the wildcard operator: `_`

###### Kotlin

Kotlin uses the when expression instead. It supports type checks and if expressions

```kotlin
data class Person(val name: String, val age: Int)
    
val person = Person("Matthew", 12)
    
val result = when {
   person.age == 12 -> println("${person.name} is 12 years old.")
   else ->  println("${person.name} is 12 years old.")
}
```

> In order to compile, a when expression has to be exhaustive. This means that every single possible case has to be matched. 
However, this can easily be achieved by using the `else` operator

### Higher Ordered Functions

#### Lambda Expressions

###### Scala

There are **alot** of different ways to write lambda expressions in scala.

```scala
val list: List[Int] = List(1, 2, 3)

list.map(_ + 1)
list map(_ + 1)
list.map(x => x + 1)
list.map((x) => x + 1)
list.map { _ + 1 }
list map { _ + 1 }
list.map { x => x + 1 }
list.map { (x) => x + 1 }
list.map(1 +)
```

###### Kotlin

Kotlin lambda expressions are always written in code blocks.

```kotlin
val list: List<Int> = listOf(1, 2, 3)

list.map { it + 1 }
list.map { x -> x + 1 }
```

#### Anonymous Functions

###### Scala (Function Literals)

Function literals are inline functions. In Scala, a lambda expression is also a function literal.

```scala
// declaring function literals
var transformer = (element: Int): Int => element + 1
val transformer2: Int => Int = _ + 1

def someFunction[A](list: List[A], transformer: A => A): List[A] = list map transformer

// using function literals as parameters
someFunction(List(1, 2, 3), transformer) // List(2,3,4)
// inlining function literals as lambda expressions
someFunction(List(1, 2, 3), (element: Int) => element + 1) // List(2,3,4)
```

###### Kotlin

An anonymous function is an inline function. A lambda expression is also an anonymous function.

```kotlin
// declaring anonymous functions
fun(string: String): String { 
    return string.toLowerCase()
}
val transformer = (element: Int): Int => element + 1

fun someFunction <T> (list: Collection<T>, transformer: (el: T) -> T): Collection<T> {
    return list.map(transformer)
}

// inlining anonymous functions as lambda expressions
someFunction(listOf(1, 2, 3), { element -> element + 1 }) // listOf(2,3,4)
// inlining anonymous functions
someFunction(listOf(1, 2, 3), fun(element: Int): Int { return element + 1 }) // listOf(2,3,4)    
```

### Collections

Both Scala and Kotlin provide a mutable and immutable collections API. 

- Mutable Collections API
    - Allows read and write operations on collections 
    - Scala creates mutable collections by using `Buffers`
    - Kotlin uses convenience methods like `m̀utableListOf(...), mutableMapOf(key to value)`
- Immutable Collections API
    - Read-Only access to collection methods
    - `scala: List(...), Map(key -> value)`
    - `kotlin: listOf(...), mapOf(key to value)`

### Operator Overloading

Both Scala and Kotlin provide Operator overloading. However, Kotlin only has a fixed amount of operators that can be overloaded.

###### Scala

In Scala, calling operators is the same as calling functions normally. Its a feature that's built in automatically because of the call syntax of methods on objects. Because method names can be whatever you want, you can define as many operators as your heart desires.

```scala
class Point(x: Int, y: Int) {
  def +(that: Point) = new Point(this.x + that.x, this.y + that.y)
  
  def -+-(that: Point) = new Point(42, 42)
}

val pointA: Point = new Point(20, 10)
val pointB: Point = new Point(10, 20)

pointA -+- pointB // Point(42, 42)
```

###### Kotlin

Kotlin on the other hand, has a limit on operators.

Overloadable operators:
- `-a` => `a.unaryMinus()`
- `+a` => `a.unaryPlus()`
- `!a` => `a.not()`
- `a++` => `a.inc()`
- `a--` => `a.dec()`
- `a + b` => `a.plus(b)`
- `a - b` => `a.minus(b)`
- `a * b` => `a.times(b)`
- `a / b` => `a.div(b)`
- `a % b` => `a.rem(b)`
- `a..b` => `a.rangeTo(b)`
- `a in b` => `b.contains(a)`
- `a !in b` => `!b.contains(a)`
- Many more (index call operators, more comparison operators...)

Defining operator logic is fairly simple:

```kotlin
data class Point(val x: Int, val y: Int)

operator fun unaryMinus() = Point(-x, -y) 

val pointA: Point = Point(10, 20) 
-pointA // Point(-10, -20)

operator fun plus(b: Point) = Point(x + b.x, y + b.y)

val pointB: Point = Point(20, 10)

pointA + pointB // Point(30, 30)
```


### Object and Method Extension

Method extensions allow you to create custom methods on an already existing types.
Object extensions allow you to create custom properties and fields on already existing types.

###### Scala

Method, object and property extensions are a feature that will be implemented with Dotty (new Scala standard lib coming up in Scala 3.0). Here is an example of how method extensions will look like:

```scala
given ListOps {
  def (xs: List[T]) second[T]: T = xs.tail.head
  def (xs: List[T]) third[T]: T = xs.tail.tail.head
}

val xs = List(1, 2, 3)

xs.second[Int] // 2
ListOps.third[Int](xs) // 3
```

###### Kotlin

Method extensions

```kotlin
fun <T1, T2> MutableMap<T1, T2>.swapValuesForKeys(key1: T1, key2: T1): MutableMap<T1, T2> {
    val tmp1: T2? = this.get(key1)
    val tmp2: T2? = this.get(key2)
        
    this.set(key1, tmp2!!)
    this.set(key2, tmp1!!)
}
    
val map: MutableMap<String, String>  = mutableMapOf("firstKey" to "firstObject", "secondKey" to "secondObject")
    
map.swapValuesForKeys("firstKey", "secondKey") // {firstKey=secondObject, secondKey=firstObject}
```

Companion Object extensions

```kotlin
class MyAmazingClass {
    companion object {
        ...
    }
    ...
}

fun MyAmazingClass.Companion.print() { println("the amazing classes companion objects print method") }

MyAmazingClass.print() // prints "the amazing classes companion objects print method"
```

Property extensions

```kotlin
val <T> List<T>.lastIndex: Int
    get() = size - 1
```

### Type Aliases

###### Scala

```scala
type BasicFormatter = (Int, String) => String

def formatEntityDescription(entity: Object, entityFormatter: BasicFormatter): String = {
  ...
}
```

###### Kotlin

```kotlin
typealias BasicFormatter = (Int, String) -> String

fun formatEntityDescription(entity: Object, entityFormatter: BasicFormatter): String {
   return ...
}


typealias ClickHandler = (ClickEvent, Component, User) -> Unit
```

## Scala Only

#### Implicit Conversions

Implicit conversions are a handy feature that allow instant conversion of objects to other objects. These implicit conversions are always one-way.

```scala
case class FooBar(int: Int)
    
FooBar(40) + 2 // compile error because no implicit conversion has been defined
    
implicit def fooBarToInteger(fooBar: FooBar): Int = fooBar.int
    
FooBar(40) + 2 // 42
    
implicit def integerToFooBar(int: Int): FooBar = FooBar(int)
    
40 + FooBar(2) // 42
    
Foo(40) + Foo(2) // 42
```

#### Structural Types

Structural Types can be helpful when declaring parameters that only have to have a specific amount of arguments.

Structural Types with a length of less than 50 characters may be used inline. Otherwise they have to be declared using the `type` keyword.

```scala
type SomeParam = {
  val someList: List[String]
  def someMethod(list: List[String])
}

def someMethod(params: SomeParam): List[String] = {
    params.someMethod(params.someList)
}
```

#### Yield Operator and For-Comprehensions

Scalas "for comprehensions" are a shorthand for multiple operations on collections involving the `foreach`, `map`, `flatMap`, `filter` or `withFilter` methods.
Under the hood, they actually get transformed into calls of these methods, meaning that any class that implements all of these methods, can be used in a for-comprehension.

```scala
for(x <- c1; y <- c2; z <- c3) { ... } 
// becomes
c1.foreach(x => c2.foreach(y => c3.foreach(z => { ... })))

for(x <- c1; y <- c2; z <- c3) yield { ... }
// becomes
c1.flatMap(x => c2.flatMap(y => c3.flatMap(z => { ... })))

for(x <- c1; if condition) yield { ... }
// becomes
c1.withFilter(x => condition).map(x => { ... })
// with a fallback to
c.filter(x => cond).map(x => {...})

```

This can look confusing. It doesn't have to be! Here's a simple example:

```scala
val names: List[String] = List("miriam", "melvin", "tom", "katharina")

val namesWithM = for(e <- names; if e.startsWith("m")) yield e // List("miriam", "melvin")
```

With more than one collection.

```scala
val namesWithM: List[String] = List("miriam")
val namesWithK: List[String] = List("konrad", "katharina")

val namePairs = for(nM <- namesWithM; nK <- namesWithK) yield s"$nM and $nK"// List("miriam und konrad", "miriam und katharina")
```

#### Macros

Macros can be used (in combination with annotations) to generate scala code at compile time.


Given the following macro reference
```scala
def assert(cond: Boolean, msg: Any) = macro Asserts.assertImpl
```

Gets transformed at compile time to
```scala
assertImpl(c)(<[ x < 10 ]>, <[ “limit exceeded” ]>)
```

The actual Macro implementation
```scala
import scala.reflect.macros.Context
import scala.language.experimental.macros

object Asserts {
  def raise(msg: Any) = throw new AssertionError(msg)
  def assertImpl(c: Context)
    (cond: c.Expr[Boolean], msg: c.Expr[Any]) : c.Expr[Unit] =
   if (assertionsEnabled)
      <[ if (!cond) raise(msg) ]>
      else
      <[ () ]>
}
```

Use cases of Macros involve dynamic code generation for advanced libraries (e.g: Type-Safe JSON Serializers, Logging libraries, etc.). 

#### Higher-Kinded Types

Higher-Kinded types are a feature in Scala that gives a programmer the ability to abstract across types that take other type parameters. This helps reduce code duplication 
for abstract code drastically.

To understand HKE we have to grasp the concepts of proper types, and first-order types first.

A proper type is a typed that contains no abstraction. For example: `String` (a String) or `List[String]` (a List of Strings) are proper types

First-order types are just types (`List, Map, Array`) that have type constructors (`List[_], Map[_, _]`) that take proper types and produce proper types (`List[Int], Map[String, Int]`).

Higher-kinded types are basically types with type constructors (ie. a type with `[_]`). Therefore first-order types are already higher-kinded types!

Abstractions over first-order types are called second-order types. An example would look like this:
```scala
    trait WithFirstOrderType[F[_]] { // F[_] represents a first-order type
      def map[A, B](fa: F[A])(f: A => B): F[B] // given a first-order type of type A, map over every single element, returning a first-order type of type B
    } 
```

#### Type Bounds
- Type Parameters and Abstract Type members may be constrained by a type bound
    - Upper Type Bounds
        - `B <: A` => `B` refers to a subtype of type `A`
        - Example:
        ```scala
          abstract class Animal {
           def name: String
          }
          
          abstract class Pet extends Animal {}
          
          class Cat extends Pet {
            override def name: String = "Cat"
          }
          
          class Dog extends Pet {
            override def name: String = "Dog"
          }
          
          class Lion extends Animal {
            override def name: String = "Lion"
          }
          
          class PetContainer[P <: Pet](p: P) {
            def pet: P = p
          }
          
          val dogContainer = new PetContainer[Dog](new Dog)
          val catContainer = new PetContainer[Cat](new Cat)
        ```
    - Lower Type Bounds
        - `B >: A` => `B` refers to a supertype of type `A`
        - Example:
         ```scala
            case class ListNode[+A](h: A, t: ListNode[A]) {
              def head: A = h
              def tail: ListNode[A] = t
              def prepend[B >: A](elem: B): ListNode[B] =
                ListNode(elem, this)
            }

            // the following becomes possible
             
            val emptyList = ListNode[Null] = ListNode(null, null) // ListNode(null, null)
            val stringList = ListNode[String] = emptyList.prepend("a string") // ListNode("a string", ListNode(null, null))
            val anyList = ListNode[Any] = stringList = stringList.prepend(1) // ListNode(1, ListNode("a string", ListNode(null, null)))
         ```

> Variances in Scala are described with the annotations `+A` (A covariant type), `-A` (A contravariant type) and `A` (An invariant type). Variance describes type relationships between subtypes 

## Kotlin Only

#### Built-In Null Safety

Kotlin supports null safety with different operators.
By default, a variable declared as a `String` cannot be assigned `null`

Kotlin supplies us with some useful operators to shorthand null checks if we happen to have to deal with them (for example in programs involving I/O).
This can be especially useful when chaining access to object values.

- The Safe Call Operator `.?`
    - Returns null if the value the operator was applied on is null
- The Elvis Operator `?:`
    - Returns the value on the right hand side as a fallback to null
- The Not-Null Assertion Operator `!!`
    - Explicitly throws a `KotlinNullPointerException` if the value the operator was applied on is null

```kotlin
var a: String = null //compilation error

var b: String? = null // null
b.length // compilation error
b?.length // null
b?.length ?: -1 // -1
b!!.length // KotlinNullPointerException
```

Kotlin also allows filtering collections of null values 
```kotlin
val nullableList = listOf(1, null).filterNotNull() // List(1)
```

Scala achieves null safety with idioms like using pattern matching with the `Option` monad or using `.flatMap` on an `Option` 
 ```scala
 val option: Option[Int] = Some(42)
 
 option match {
  case Some(_) => println("Received value: " + _)
  case None => println("No value received")
 }
```
```scala
val options: List[Option[Int]] = List(Some(1), Some(2), Some(3), None)

options.flatMap(v => v) // List(1, 2, 3)
```
#### Smart Casts

If you do a type check on a value in Kotlin, the compiler automatically casts it to the type you checked the value on.

```kotlin
interface Pet { }

open class Cat(val name: String): Pet
open class Dog(val name: String): Pet

fun dogMachine(pet: Pet): Dog {
    if(pet is Dog) {
        return pet
    } else {
        throw Error("Not a dog!")
    }
} 

dogMachine(Dog("Brian Griffin")) // Dog("Brian Griffin")
```

#### Reified Type Parameters

Kotlin allows inline functions to be able to reify type parameters to effectively use reflection.

```kotlin
inline fun <reified T> membersOf() = T::class.members
```
